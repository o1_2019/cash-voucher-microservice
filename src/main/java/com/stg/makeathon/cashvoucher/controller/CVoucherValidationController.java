package com.stg.makeathon.cashvoucher.controller;

import com.stg.makeathon.cashvoucher.domain.CashVoucher;
import com.stg.makeathon.cashvoucher.service.CVoucherValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CVoucherValidationController {

    @Autowired
    CVoucherValidationService validationService;

    @GetMapping("/cashvoucher/validation")
    public CashVoucher getVoucher(@RequestParam("userName") int uName,@RequestParam("voucherID") String voucherId){
        CashVoucher voucher=validationService.getVoucherDetail(uName,voucherId);
        return voucher;
    }
}
