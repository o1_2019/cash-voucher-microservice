package com.stg.makeathon.cashvoucher.controller;

import com.stg.makeathon.cashvoucher.dto.InvalidationDto;
import com.stg.makeathon.cashvoucher.dto.InvalidationResponseDto;
import com.stg.makeathon.cashvoucher.service.CVoucherInvalidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CVoucherInvalidationController {
    @Autowired
    CVoucherInvalidationService invalidationService;

    @PostMapping("/cashvoucher/invalidation")
    public InvalidationResponseDto invalidateVoucher(@RequestParam("voucherId") String voucherId){
        InvalidationDto invalidation=new InvalidationDto();
        System.out.println("Invalidatio" + voucherId);
        invalidation.setVoucherId(voucherId);

        InvalidationResponseDto responseDto=invalidationService.inValidateVoucher(invalidation);
        responseDto.setPaymentId("CP000001");
        return responseDto;
    }
}
