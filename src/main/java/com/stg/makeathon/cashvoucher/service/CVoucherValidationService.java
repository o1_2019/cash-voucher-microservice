package com.stg.makeathon.cashvoucher.service;

import com.stg.makeathon.cashvoucher.domain.CashVoucher;
import com.stg.makeathon.cashvoucher.repository.CashVoucherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service("validationService")
public class CVoucherValidationService {
    @Autowired
    CashVoucherRepository repository;

    public CashVoucher getVoucherDetail(int userName , String voucherId){
        List<CashVoucher> voucherList=repository.findByUserName(userName);

        for (int i=0;i<voucherList.size();i++){
            if (voucherList.get(i).getVoucherId().equals(voucherId)){
                if (voucherList.get(i).isRedeem()){
                    return voucherList.get(i);
                }
            }
        }

        return null;
    }
}
