package com.stg.makeathon.cashvoucher.service;

import com.stg.makeathon.cashvoucher.domain.CashVoucher;
import com.stg.makeathon.cashvoucher.dto.InvalidationDto;
import com.stg.makeathon.cashvoucher.dto.InvalidationResponseDto;
import com.stg.makeathon.cashvoucher.repository.CashVoucherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("invalidationService")
public class CVoucherInvalidationService {
    @Autowired
    CashVoucherRepository repository;
    public InvalidationResponseDto inValidateVoucher(InvalidationDto invalidationDto){
        InvalidationResponseDto response=new InvalidationResponseDto();
        CashVoucher voucher=repository.findByVoucherId(invalidationDto.getVoucherId());

        voucher.setRedeem(true);
        response.setVoucherId(invalidationDto.getVoucherId());

        repository.save(voucher);
        response.setInvalidate(true);
        return response;


    }
}
