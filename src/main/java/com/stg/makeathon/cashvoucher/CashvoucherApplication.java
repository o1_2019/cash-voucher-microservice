package com.stg.makeathon.cashvoucher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CashvoucherApplication {

	public static void main(String[] args) {
		SpringApplication.run(CashvoucherApplication.class, args);
	}

}
