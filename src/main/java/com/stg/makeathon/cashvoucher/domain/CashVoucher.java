package com.stg.makeathon.cashvoucher.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "cash_voucher" , schema = "public")
public class CashVoucher {
    @Id
    @Column(name = "voucherid")
    String voucherId;
    @Column(name = "username")
    int userName;
    @Column(name="amount")
    long amount;
    @Column(name="isredeem")
    boolean isRedeem;
    @Column(name = "expirydate")
    Date expiryDate;

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public int getUserName() {
        return userName;
    }

    public void setUserName(int userName) {
        this.userName = userName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public boolean isRedeem() {
        return isRedeem;
    }

    public void setRedeem(boolean redeem) {
        isRedeem = redeem;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}
