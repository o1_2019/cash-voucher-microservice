package com.stg.makeathon.cashvoucher.dto;

import com.stg.makeathon.cashvoucher.domain.CashVoucher;

public class InvalidationDto {
    String voucherId;
    int userName;
    String paymentId;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public int getUserName() {
        return userName;
    }

    public void setUserName(int userName) {
        this.userName = userName;
    }


}
