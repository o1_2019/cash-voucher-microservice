package com.stg.makeathon.cashvoucher.dto;

public class InvalidationResponseDto {
    String voucherId;
    boolean isInvalidate;
    String paymentId;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public boolean isInvalidate() {
        return isInvalidate;
    }

    public void setInvalidate(boolean invalidate) {
        isInvalidate = invalidate;
    }
}
