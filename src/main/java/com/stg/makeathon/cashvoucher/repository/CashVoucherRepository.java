package com.stg.makeathon.cashvoucher.repository;

import com.stg.makeathon.cashvoucher.domain.CashVoucher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CashVoucherRepository extends JpaRepository<CashVoucher,String> {
    List<CashVoucher> findByUserName(int userName);
    CashVoucher findByVoucherId(String voucherId);

}
